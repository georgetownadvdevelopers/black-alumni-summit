<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

//SERVER CONNECTION INFO
$serverName = "guadvdb";
require("connection.php");
$cnx = sqlsrv_connect($serverName, $connectionInfo);
// echo "cnx = ";
// var_dump($cnx);
// GIVE IT A QUERY AND A CONNECTIONR REFERENCE, IT SPITS OUT THE QUERY RESULT
	function rtnQuery($qry, $conn){
		$getResult;
		if($conn){
			$getResult = sqlsrv_query($conn, $qry) or die(print_r(sqlsrv_errors(SQLSRV_ERR_ALL)));
			return $getResult;
		}else{
			echo "Connection could not be established. Try again later.";
			die( print_r(sqlsrv_errors(), true));
		}
	}

	function submitAlumInfo($f, $conn){
		$Alum = $f;
		foreach($Alum as $key => $value){
			$Alum[$key] = addslashes($value);
			// echo "\nkey: ".$key." value: ".addslashes($value);
		}
		// print_r($Alum);
		if(!empty($Alum['gusy'])){
		$Alum['gusy'] = str_replace("'", "''", $Alum['gusy']);
		}else{
		$Alum['gusy'] = NULL;
		}
		if(empty($Alum['industry'])){
			$Alum['industry'] = NULL;
		}
		date_default_timezone_set('America/New_York');
		$subtime = date("Y-m-d H:i:s");

		$qry = "INSERT INTO dbo.BAS ([type], [first_name], [last_name], [gusy], [email], [occupation], [industry], [time]) VALUES ('{$Alum['type']}', '{$Alum['fname']}', '{$Alum['lname']}', '{$Alum['gusy']}', '{$Alum['email']}', '{$Alum['occupation']}', '{$Alum['industry']}', '{$subtime}')";
		$result = rtnQuery($qry, $conn);
		// var_dump($result);
	}

if(isset($_POST['submit'])){
	if($_POST['submit'] == 'true'){
		// echo "submitted... processing... ";
		// print_r($_POST['form']);
		if($_POST['form'] == "frm_nominate"){
			// NOMINEE INFORMATION SUBMITTED
			$fields;
			$test;
			$fields['type'] = "nominate";
			foreach($_POST['fields'] as $value){
				// echo "\nvalue: ";
				if(empty($value['value'])){
					// echo $value['name']." is empty";
					$fields[$value['name']] = NULL;
				}else{
					// echo $value['name']." is not empty";
					$fields[$value['name']] = addslashes($value['value']);
				};
			}
			// echo "\n";
			// print_r($fields);
			submitAlumInfo($fields, $cnx);
		}else if($_POST['form'] == "frm_inform"){
			// ALUM INFORMATION SUBMITTED
			$fields;
			$fields['type'] = "updates";
			foreach($_POST['fields'] as $value){
				// print_r($_POST['fields']);
				// echo "\nvalue: ";
				if(!isset($value['value'])){
					// echo $value['name']." is empty";
					$fields[$value['name']] == NULL;
				}else{
					// echo $value['name']." is not empty";
					$fields[$value['name']] = addslashes($value['value']);
				};
				$fields['occupation'] = NULL;
				$fields['industry'] = NULL;
			}
			// echo "\n";
			// print_r($fields);
			submitAlumInfo($fields, $cnx);
		}

	}
}
?>