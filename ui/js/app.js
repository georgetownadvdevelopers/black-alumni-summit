$(document).ready(function(){

  //Disable Clicking if has class .disabled //
$(".disabled a").click(function(){
  event.preventDefault();
});


//Added Modal Gallery Markup //
    $('.thumbnail').click(function(){
        event.preventDefault();
          $('.modal-body').empty();
      	$($(this).parents('div').html()).appendTo('.modal-body');
      	$('#myModal').modal({show:true});
    });

//Schedule Scrolling //
  $("#friday").click(function(){
      $("#fri-toggle").toggle(650);
  });
  $("#saturday").click(function(){
      $("#sat-toggle").toggle(650);
  });
  $("#sunday").click(function(){
      $("#sun-toggle").toggle(650);
  });

      $(function() {
          $(".rslides").responsiveSlides({
              timeout:6000,
              nav:true,
              pause:true,
              random:true
          });
      });
  function scrollNav() {
    $('.nav a').click(function(){
      //Toggle Class
      $(".active").removeClass("active");
      $(this).closest('li').addClass("active");
      var theClass = $(this).attr("class");
      $('.'+theClass).parent('li').addClass('active');
      //Animate
      $('html, body').stop().animate({
          scrollTop: $( $(this).attr('href') ).offset().top - 50
      }, 400);
      return false;
    });
    /*$('.scrollTop a').scrollTop();*/
  }
  scrollNav();

  	var currForm;
  	$('#about').find('.formOpen').click(function(e){
  		if(typeof popup !== "undefined"){
  			popup.dialog("close");
  		}
  		var tgt = e.currentTarget.name;
  		currForm = tgt;
  		$(".errormsg").html("");
  		$('.frm').hide();
  		if(Modernizr.mq("only screen and (max-width: 768px)")){
  			popupW = 300;
  		}else{
  			popupW = 600;
  		}
  		popup = $("#frm_"+tgt).dialog({
  			width:popupW
  		});
  		if(tgt == "schedule"){
  			// popup = $("<p style='text-align:center;'>October 2015, more information coming soon.</p>").dialog({
  			// 	width:popupW
  			// });
  		}
  		if(tgt == "support"){
  			console.log('support');
  			window.open('https://secure.alumni.georgetown.edu/olc/pub/GTW/onlinegiving/showGivingForm.jsp?form_id=75315&referrer_type=UP&GTW_giftinfo_referralcode=ombas&GTW_giftinfo_giftdesignation1=other&GTW_giftinfo_giftdesignationother1=Black+Alumni+Summit', '_blank');
  		}
  	});
  	function validation(){ //VALIDATE FORM FIELDS!
  		var openForm = $("#frm_"+currForm);
  		var nominateArr = ["nominee_fname", "nominee_lname", "nominee_email"];
  		var informArr = ["inform_fname", "inform_lname", "inform_email"];
  		var errorArr = [];
  		var validFlag = false;
  		switch(currForm){
  			case "nominate":
  				for(each in nominateArr){
  					if($("#"+nominateArr[each]).val() !== ""){
  						validFlag = true;
  					}else{
  						validFlag = false;
  						errorArr.push(nominateArr[each]);
  					}
  				}
  			break;
  			case "inform":
  				for(each in informArr){
  					if($("#"+informArr[each]).val() !== ""){
  						validFlag = true;
  					}else{
  						validFlag = false;
  						errorArr.push(informArr[each]);
  					}
  				}
  			break;
  		};
  		// console.log("validFlag: "+validFlag);
  		// console.log("\nerrors: "+errorArr);
  		for(each in errorArr){
  			$("#"+errorArr[each]).addClass('error');
  			$(".errormsg").html("Please fill in all required fields marked in red.");
  		}
  		if(!validFlag){
  			$(".ui-dialog[aria-describedby=frm_"+currForm+"]").effect("shake", {'distance':10});
  		}
  		return validFlag;
  	};
  	$('.frm').submit(function(e){
  		e.preventDefault();
  		if(validation()){
  			var tgt = $(e.target).attr('id');
  			var frmData = $("#"+tgt).serializeArray();
  			// console.log(tgt);
  			// console.log($('#frm').serializeArray());
  			// console.info(frmData);
  			$.post('BAS.php', {'submit':true, 'form':tgt, 'fields':frmData}, function(data){
  				console.log(data);
  			});
  			var origForm = $("#"+tgt).html();
  			$("#"+tgt).html("<p>Thank you for your submission.</p>");
  			var to = setTimeout(function(){
  				$("#"+tgt).html(origForm);
  				popup.dialog("close");
  			}, 3500);
  		}
  	})
  var nav = responsiveNav(".nav-collapse");

//Paralax Scrolling//
  $('div[data-type="background"]').each(function(){
                     var $bgobj = $(this); // assigning the object

                   $(window).scroll(function() {
                             var yPos = -($(window).scrollTop() / $bgobj.data('speed'));

                             // Put together our final background position
                             var coords = '50% '+ yPos + 'px';

                             // Move the background
                             $bgobj.css({ backgroundPosition: coords });
                         });

       });


            });
